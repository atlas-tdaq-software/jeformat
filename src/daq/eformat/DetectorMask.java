package daq.eformat;

import java.math.BigInteger;
import java.util.BitSet;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * Utility class to build and decode the detector mask.
 * <p>
 * The current size (in bits) of the detector mask is 128. Methods are available to (de)serialize the detector mask to a string
 * representation: that string is made up of hexadecimal characters representing the detector mask bits (i.e, the string is made up of 32
 * characters).
 */

public class DetectorMask {
    private final static Map<Integer, SUBDETECTOR> idMap = new HashMap<Integer, DetectorMask.SUBDETECTOR>();
    private final static Map<Integer, SUBDETECTOR> positionMap = new HashMap<Integer, DetectorMask.SUBDETECTOR>();

    // Static initializer
    static {
        for(final SUBDETECTOR s : SUBDETECTOR.values()) {
            DetectorMask.idMap.put(Integer.valueOf(s.detID), s);
            DetectorMask.positionMap.put(Integer.valueOf(s.position), s);
        }
    }

    /**
     * Enumeration for detectors the detector mask is referring to
     */
    public enum DETECTOR {
        PIXEL,
        SCT,
        TRT,
        LAR,
        TILE,
        MDT,
        RPC,
        TGC,
        CSC,
        MICROMEGA,
        STGC,
        DAQ,
        LVL1,
        FTK,
        BCM,
        LUCID,
        ZDC,
        ALFA,
        AFP,
        HLT,
        OTHER_DETECTORS;
    }

    /**
     * Enumeration for sub-detectors making up the detector mask
     * <p>
     * Any bit in the detector mask corresponds to an item in this enumeration.
     */
    public enum SUBDETECTOR {
        PIXEL_BARREL(0, 0x11, DETECTOR.PIXEL),
        PIXEL_DISK(1, 0x12, DETECTOR.PIXEL),
        PIXEL_B_LAYER(2, 0x13, DETECTOR.PIXEL),
        OTHER(3, 0xff, DETECTOR.OTHER_DETECTORS),
        SCT_BARREL_A_SIDE(4, 0x21, DETECTOR.SCT),
        SCT_BARREL_C_SIDE(5, 0x22, DETECTOR.SCT),
        SCT_ENDCAP_A_SIDE(6, 0x23, DETECTOR.SCT),
        SCT_ENDCAP_C_SIDE(7, 0x24, DETECTOR.SCT),
        TRT_BARREL_A_SIDE(8, 0x31, DETECTOR.TRT),
        TRT_BARREL_C_SIDE(9, 0x32, DETECTOR.TRT),
        TRT_ENDCAP_A_SIDE(10, 0x33, DETECTOR.TRT),
        TRT_ENDCAP_C_SIDE(11, 0x34, DETECTOR.TRT),
        LAR_EM_BARREL_A_SIDE(12, 0x41, DETECTOR.LAR),
        LAR_EM_BARREL_C_SIDE(13, 0x42, DETECTOR.LAR),
        LAR_EM_ENDCAP_A_SIDE(14, 0x43, DETECTOR.LAR),
        LAR_EM_ENDCAP_C_SIDE(15, 0x44, DETECTOR.LAR),
        LAR_HAD_ENDCAP_A_SIDE(16, 0x45, DETECTOR.LAR),
        LAR_HAD_ENDCAP_C_SIDE(17, 0x46, DETECTOR.LAR),
        LAR_FCAL_A_SIDE(18, 0x47, DETECTOR.LAR),
        LAR_FCAL_C_SIDE(19, 0x48, DETECTOR.LAR),
        TILECAL_BARREL_A_SIDE(20, 0x51, DETECTOR.TILE),
        TILECAL_BARREL_C_SIDE(21, 0x52, DETECTOR.TILE),
        TILECAL_EXT_A_SIDE(22, 0x53, DETECTOR.TILE),
        TILECAL_EXT_C_SIDE(23, 0x54, DETECTOR.TILE),
        MUON_MDT_BARREL_A_SIDE(24, 0x61, DETECTOR.MDT),
        MUON_MDT_BARREL_C_SIDE(25, 0x62, DETECTOR.MDT),
        MUON_MDT_ENDCAP_A_SIDE(26, 0x63, DETECTOR.MDT),
        MUON_MDT_ENDCAP_C_SIDE(27, 0x64, DETECTOR.MDT),
        MUON_RPC_BARREL_A_SIDE(28, 0x65, DETECTOR.RPC),
        MUON_RPC_BARREL_C_SIDE(29, 0x66, DETECTOR.RPC),
        MUON_TGC_ENDCAP_A_SIDE(30, 0x67, DETECTOR.TGC),
        MUON_TGC_ENDCAP_C_SIDE(31, 0x68, DETECTOR.TGC),
        MUON_CSC_ENDCAP_A_SIDE(32, 0x69, DETECTOR.CSC),
        MUON_CSC_ENDCAP_C_SIDE(33, 0x6A, DETECTOR.CSC),
        TDAQ_CALO_PREPROC(34, 0x71, DETECTOR.LVL1),
        TDAQ_CALO_CLUSTER_PROC_DAQ(35, 0x72, DETECTOR.LVL1),
        TDAQ_CALO_CLUSTER_PROC_ROI(36, 0x73, DETECTOR.LVL1),
        TDAQ_CALO_JET_PROC_DAQ(37, 0x74, DETECTOR.LVL1),
        TDAQ_CALO_JET_PROC_ROI(38, 0x75, DETECTOR.LVL1),
        TDAQ_MUON_CTP_INTERFACE(39, 0x76, DETECTOR.LVL1),
        TDAQ_CTP(40, 0x77, DETECTOR.LVL1),
        TDAQ_L2SV(41, 0x78, DETECTOR.DAQ),
        TDAQ_SFI(42, 0x79, DETECTOR.DAQ),
        TDAQ_SFO(43, 0x7a, DETECTOR.DAQ),
        TDAQ_LVL2(44, 0x7b, DETECTOR.DAQ),
        TDAQ_HLT(45, 0x7c, DETECTOR.HLT),
        FORWARD_BCM(46, 0x81, DETECTOR.BCM),
        FORWARD_LUCID(47, 0x82, DETECTOR.LUCID),
        FORWARD_ZDC(48, 0x83, DETECTOR.ZDC),
        FORWARD_ALPHA(49, 0x84, DETECTOR.ALFA),
        TRT_ANCILLARY_CRATE(50, 0x30, DETECTOR.TRT),
        TILECAL_LASER_CRATE(51, 0x50, DETECTOR.TILE),
        MUON_ANCILLARY_CRATE(52, 0x60, DETECTOR.MDT),
        TDAQ_BEAM_CRATE(53, 0x70, DETECTOR.LVL1),
        TDAQ_FTK(54, 0x7f, DETECTOR.FTK),
        OFFLINE(55, 0x01, DETECTOR.OTHER_DETECTORS),
        TDAQ_CALO_TOPO_PROC(56, 0x91, DETECTOR.LVL1),
        TDAQ_CALO_DIGITAL_PROC(57, 0x92, DETECTOR.LVL1),
        TDAQ_CALO_FEAT_EXTRACT_DAQ(58, 0x93, DETECTOR.LVL1),
        TDAQ_CALO_FEAT_EXTRACT_ROI(59, 0x94, DETECTOR.LVL1),
        MUON_MMEGA_ENDCAP_A_SIDE(60, 0x6B, DETECTOR.MICROMEGA),
        MUON_MMEGA_ENDCAP_C_SIDE(61, 0x6C, DETECTOR.MICROMEGA),
        PIXEL_IBL(62, 0x14, DETECTOR.PIXEL),
        PIXEL_DBM(63, 0x15, DETECTOR.PIXEL),
        FORWARD_AFP(64, 0x85, DETECTOR.AFP),
        LAR_EM_BARREL_ENDCAP_A_SIDE(65, 0x49, DETECTOR.LAR),
        LAR_EM_BARREL_ENDCAP_C_SIDE(66, 0x4a, DETECTOR.LAR),
        LAR_EM_HAD_ENDCAP_A_SIDE(67, 0x4b, DETECTOR.LAR),
        LAR_EM_HAD_ENDCAP_C_SIDE(68, 0x4c, DETECTOR.LAR),
        MUON_STGC_ENDCAP_A_SIDE(69, 0x6d, DETECTOR.STGC),          
        MUON_STGC_ENDCAP_C_SIDE(70, 0x6e, DETECTOR.STGC);
        
        private final int position;
        private final int detID;
        private final DETECTOR det;

        /**
         * Constructor
         * 
         * @param position The position in the detector mask (e.g., the corresponding bit)
         * @param id The sub-detector's unique ID
         * @param det The detector the sub-detector belongs to
         */
        private SUBDETECTOR(final int position, final int id, final DETECTOR det) {
            assert (position < DetectorMask.DETMASK_SIZE);

            this.position = position;
            this.detID = id;
            this.det = det;
        }

        /**
         * It returns the sub-detector unique ID
         * 
         * @return The sub-detector unique ID
         */
        public int id() {
            return this.detID;
        }

        /**
         * It returns the position of the sub-detector in the detector mask (e.g., the corresponding bit)
         * 
         * @return The position of the sub-detector in the detector mask (e.g., the corresponding bit)
         */
        public int position() {
            return this.position;
        }

        /**
         * It returns the detector the sub-detector belongs to
         * 
         * @return The detector the sub-detector belongs to
         */
        public DETECTOR detector() {
            return this.det;
        }
    }

    /**
     * The size of the bit mask used to represent the detector mask
     */
    private final static int DETMASK_SIZE = 128;

    /**
     * Number of characters needed to give a string representation of the detector mask where each character represents an hexadecimal
     * number
     */
    private final static int STRING_REPR_LENGTH = DetectorMask.DETMASK_SIZE / 4; // 32

    /**
     * The bit set implementing the detector mask
     */
    private final BitSet mask;

    /**
     * Constructor
     * 
     * @param subDets List of sub-detectors set as "active" in the detector mask
     */
    public DetectorMask(final Set<SUBDETECTOR> subDets) {
        this.mask = new BitSet(DetectorMask.DETMASK_SIZE);

        for(final SUBDETECTOR s : subDets) {
            this.mask.set(s.position());
        }
    }

    /**
     * Constructor
     * <p>
     * It creates an "empty" detector mask where none of the sub-detectors is set active
     */
    public DetectorMask() {
        this(EnumSet.noneOf(SUBDETECTOR.class));
    }

    /**
     * Constructor
     * <p>
     * It creates the detector mask from the underlining bit set
     * 
     * @param bm
     */
    private DetectorMask(final BitSet bm) {
        this.mask = bm;
    }

    /**
     * It sets the <em>subDet</em> sub-detector active
     * 
     * @param subDet The sub-detector
     */
    public void set(final SUBDETECTOR subDet) {
        this.mask.set(subDet.position());
    }

    /**
     * It sets the <em>subDets</em> sub-detectors active
     * 
     * @param subDets The sub-detectors
     */
    public void set(final Set<SUBDETECTOR> subDets) {
        for(final SUBDETECTOR s : subDets) {
            this.mask.set(s.position());
        }
    }

    /**
     * It sets active the sub-detector the <em>robid</em> is associated to
     * 
     * @param robid The <b>full ROBID</b> of the data channel
     * @throws IllegalArgumentException <em>robid</em> is not valid (i.e., it does not correspond to any sub-detector)
     */
    public void set(final int robid) {
        final int subID = (robid >> 16) & 0xFF;

        final SUBDETECTOR det = DetectorMask.idMap.get(Integer.valueOf(subID));
        if(det != null) {
            this.set(det);
        } else {
            throw new IllegalArgumentException("The source identifier " + robid + " is not valid/supported");
        }
    }

    /**
     * It sets the <em>subDet</em> sub-detector as not active
     * 
     * @param subDet The sub-detector
     */
    public void unset(final SUBDETECTOR subDet) {
        this.mask.clear(subDet.position());
    }

    /**
     * It sets the <em>subDets</em> sub-detectors as not active
     * 
     * @param subDets The sub-detectors
     */
    public void unset(final Set<SUBDETECTOR> subDets) {
        for(final SUBDETECTOR s : subDets) {
            this.mask.clear(s.position());
        }
    }

    /**
     * It sets as no more active the sub-detector the <em>robid</em> is associated to
     * 
     * @param roibid The <b>full ROBID</b> of the data channel
     * @throws IllegalArgumentException <em>robid</em> is not valid (i.e., it does not correspond to any sub-detector)
     */
    public void unset(final int roibid) {
        final int subID = (roibid >> 16) & 0xFF;
        final SUBDETECTOR det = DetectorMask.idMap.get(Integer.valueOf(subID));
        if(det != null) {
            this.unset(det);
        } else {
            throw new IllegalArgumentException("The source identifier " + roibid + " is not valid/supported");
        }
    }

    /**
     * It changes the status of the <em>subDet</em> sub-detector
     * 
     * @param subDet The sub-detector
     */
    public void flip(final SUBDETECTOR subDet) {
        this.mask.flip(subDet.position());
    }

    /**
     * It changes the status of the <em>subDet</em> sub-detector
     * 
     * @param subDets The sub-detectors
     */
    public void flip(final Set<SUBDETECTOR> subDets) {
        for(final SUBDETECTOR s : subDets) {
            this.mask.flip(s.position());
        }
    }

    /**
     * It changes the status of the sub-detector the <em>robid</em> is associated to
     * 
     * @param robid The <b>full ROBID</b> of the data channel
     * @throws IllegalArgumentException <em>robid</em> is not valid (i.e., it does not correspond to any sub-detector)
     */
    public void flip(final int robid) {
        final int subID = (robid >> 16) & 0xFF;
        final SUBDETECTOR det = DetectorMask.idMap.get(Integer.valueOf(subID));
        if(det != null) {
            this.flip(det);
        } else {
            throw new IllegalArgumentException("The source identifier " + robid + " is not valid/supported");
        }
    }

    /**
     * It checks whether a sub-detector is active or not
     * 
     * @param subDet The sub-detector
     * @return <em>true</em> if the sub-detector is active
     */
    public boolean isSet(final SUBDETECTOR subDet) {
        return this.mask.get(subDet.position());
    }

    /**
     * It checks whether the sub-detector associated to <em>roib</em> is active or not
     * 
     * @param robid The <b>full ROBID</b> of the data channel
     * @return <em>true</em> if the sub-detector is active
     * @throws IllegalArgumentException <em>robid</em> is not valid (i.e., it does not correspond to any sub-detector)
     */
    public boolean isSet(final int robid) {
        final int subID = (robid >> 16) & 0xFF;
        final SUBDETECTOR det = DetectorMask.idMap.get(Integer.valueOf(subID));
        if(det != null) {
            return this.isSet(det);
        }

        throw new IllegalArgumentException("The source identifier " + robid + " is not valid/supported");
    }

    /**
     * It returns a bit set representing the current status of the various sub-detectors
     * 
     * @return A bit set representing the current status of the various sub-detectors
     * @see SUBDETECTOR#position()
     */
    public BitSet get() {
        return this.mask.get(0, DetectorMask.DETMASK_SIZE);
    }

    /**
     * It returns all the active sub-detectors
     * 
     * @return A set containing all the active sub-detectors
     */
    public Set<SUBDETECTOR> getSubdetectors() {
        final EnumSet<SUBDETECTOR> sl = EnumSet.noneOf(SUBDETECTOR.class);

        for(int i = this.mask.nextSetBit(0); i >= 0; i = this.mask.nextSetBit(i + 1)) {
            sl.add(DetectorMask.positionMap.get(Integer.valueOf(i)));
        }

        return sl;
    }

    /**
     * It returns all the active detectors
     * 
     * @return A set containing all the active detectors
     */
    public Set<DETECTOR> getDetectors() {
        final EnumSet<DETECTOR> dl = EnumSet.noneOf(DETECTOR.class);

        for(int i = this.mask.nextSetBit(0); i >= 0; i = this.mask.nextSetBit(i + 1)) {
            dl.add(DetectorMask.positionMap.get(Integer.valueOf(i)).detector());
        }

        return dl;
    }

    /**
     * It returns a string representation of the detector mask
     * 
     * @return A string representation of the detector mask
     */
    @Override
    public String toString() {
        final byte[] b = this.mask.toByteArray(); // little-endian
        final byte[] swap = new byte[b.length]; // big-endian
        for(int idx = 0; idx < b.length; ++idx) {
            swap[b.length - 1 - idx] = b[idx];
        }

        // Keep the "1" parameter, otherwise it will not work (i.e., keep it positive)
        final BigInteger bi = new BigInteger(1, swap);
        return String.format("%" + DetectorMask.STRING_REPR_LENGTH + "s", bi.toString(16)).replace(' ', '0');
    }

    /**
     * It builds a detector mask starting from its string representation
     * 
     * @param dm The string representation of the detector mask
     * @return The detector mask object
     * @throws IllegalArgumentException @em dm is not a valid detector mask
     */
    public static DetectorMask decode(final String dm) {
        if(dm.length() != DetectorMask.STRING_REPR_LENGTH) {
            throw new IllegalArgumentException("The detector mask has wrong size: it should be " + DetectorMask.STRING_REPR_LENGTH
                                               + " while it is " + dm.length());
        }

        try {
            // Keep the "+" sign, otherwise it will not work (i.e., keep it positive)
            final BigInteger n = new BigInteger("+" + dm, 16);

            // The bytes coming from the BigInteger are in big-endian order
            // But the BitSet constructor wants the bytes in little-endian order
            final byte[] b = n.toByteArray(); // big-endian
            final byte[] swap = new byte[b.length]; // little-endian
            for(int idx = 0; idx < b.length; ++idx) {
                swap[b.length - 1 - idx] = b[idx];
            }

            return new DetectorMask(BitSet.valueOf(swap));
        }
        catch(final NumberFormatException ex) {
            throw new IllegalArgumentException("\"" + dm + "\" is not a valid detector mask: " + ex.toString(), ex);
        }
    }

    /**
     * It converts a detector ID in the corresponding {@link SUBDETECTOR} item
     * 
     * @param detId The unique detector ID
     * @return The corresponding {@link SUBDETECTOR} item
     * @throws IllegalArgumentException The detector ID is not valid
     */
    public static SUBDETECTOR fromDetectorID(final int detId) {
        final SUBDETECTOR det = DetectorMask.idMap.get(Integer.valueOf(detId));
        if(det == null) {
            throw new IllegalArgumentException("The detector with ID " + detId + " is not valid/supported");
        }
        
        return det;
    }
    
    // public static void main(final String[] args) {
    // final DetectorMask dm = new DetectorMask(EnumSet.of(SUBDETECTOR.PIXEL_DBM));
    // System.out.println(dm.toString());
    //
    // final DetectorMask dm_ = DetectorMask.decode(dm.toString());
    // System.out.println(dm_.toString());
    //
    // final DetectorMask dmm = new DetectorMask();
    // dmm.set(5654 | 18 << 16);
    // System.err.println(dmm.getSubdetectors());
    // }
}
